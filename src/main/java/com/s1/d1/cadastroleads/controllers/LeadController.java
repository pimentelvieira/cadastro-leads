package com.s1.d1.cadastroleads.controllers;

import com.s1.d1.cadastroleads.models.Lead;
import com.s1.d1.cadastroleads.models.Produto;
import com.s1.d1.cadastroleads.services.LeadService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/leads")
public class LeadController {

    @Autowired
    private LeadService leadService;

    @GetMapping("/{id}")
    public Lead buscarPorIndice(@PathVariable Integer id) {
        Optional<Lead> lead = this.leadService.buscarPorId(id);

        if (lead.isPresent()) {
            return lead.get();
        } else {
            throw new ResponseStatusException(HttpStatus.NO_CONTENT);
        }
    }

    @PostMapping
    public ResponseEntity<Lead> salvarLead(@RequestBody @Valid Lead lead) {
        List<Integer> produtosId = new ArrayList<Integer>();

        for (Produto produto : lead.getProdutos()) {
            produtosId.add(produto.getId());
        }

        Iterable<Produto> produtos = this.leadService.buscarTodosProdutos(produtosId);
        lead.setProdutos((List) produtos);
        Lead leadCriado = leadService.adicionarLead(lead);

        return ResponseEntity.status(HttpStatus.CREATED).body(leadCriado);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity deletarLead(@PathVariable Integer id) {
        Optional<Lead> lead = this.leadService.buscarPorId(id);

        if (lead.isPresent()) {
            this.leadService.deletarLead(id);
            return ResponseEntity.status(HttpStatus.OK).build();
        } else {
            throw new ResponseStatusException(HttpStatus.NO_CONTENT, "Lead não encontrado para exclusão");
        }
    }

    @PutMapping("/{id}")
    public ResponseEntity<Lead> atualizarLead(@PathVariable Integer id, @RequestBody @Valid Lead lead) {
        Optional<Lead> leadRetornada = this.leadService.buscarPorId(id);

        if (leadRetornada.isPresent()) {
            lead.setId(id);
            Lead leadAtualizada = this.leadService.atualizarLead(lead);
            return ResponseEntity.status(HttpStatus.OK).body(leadAtualizada);
        } else {
            throw new ResponseStatusException(HttpStatus.NO_CONTENT, "Lead não encontrado para atualização");
        }
    }
}
