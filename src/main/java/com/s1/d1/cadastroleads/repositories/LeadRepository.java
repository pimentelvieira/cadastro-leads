package com.s1.d1.cadastroleads.repositories;

import com.s1.d1.cadastroleads.models.Lead;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface LeadRepository extends CrudRepository<Lead, Integer> {

}
