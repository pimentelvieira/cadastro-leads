package com.s1.d1.cadastroleads;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CadastroLeadsApplication {

	public static void main(String[] args) {
		SpringApplication.run(CadastroLeadsApplication.class, args);
	}

}
