package com.s1.d1.cadastroleads.models;

import com.s1.d1.cadastroleads.enums.TipoLead;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;

@Entity
public class Lead {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @NotNull
    @Size(min = 8, max = 100, message = "Nome deve ter no minimo 8 caracteres e no máximo 100 caracteres")
    private String nome;

    @Email(message = "Email inválido")
    private String email;

    private TipoLead tipo;

    @ManyToMany
    private List<Produto> produtos;

    public Lead() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public TipoLead getTipo() {
        return tipo;
    }

    public void setTipo(TipoLead tipo) {
        this.tipo = tipo;
    }

    public List<Produto> getProdutos() {
        return produtos;
    }

    public void setProdutos(List<Produto> produtos) {
        this.produtos = produtos;
    }
}
