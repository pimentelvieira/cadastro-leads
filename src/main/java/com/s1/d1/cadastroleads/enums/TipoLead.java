package com.s1.d1.cadastroleads.enums;

public enum TipoLead {
    QUENTE,
    ORGANICO,
    FRIO
}
