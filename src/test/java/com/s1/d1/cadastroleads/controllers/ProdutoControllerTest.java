package com.s1.d1.cadastroleads.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.s1.d1.cadastroleads.enums.TipoLead;
import com.s1.d1.cadastroleads.models.Lead;
import com.s1.d1.cadastroleads.models.Produto;
import com.s1.d1.cadastroleads.services.ProdutoService;
import org.hamcrest.CoreMatchers;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.Arrays;
import java.util.Optional;

@WebMvcTest(ProdutoController.class)
public class ProdutoControllerTest {

    @MockBean
    private ProdutoService produtoService;

    @Autowired
    private MockMvc mockMvc;

    private Produto produto;

    private ObjectMapper objectMapper;

    @BeforeEach
    public void inicializar() {
        produto = new Produto();
        objectMapper = new ObjectMapper();

        produto.setId(2);
        produto.setDescricao("Produto teste");
        produto.setNome("Nome Produto teste");
        produto.setPreco(80.9);
    }

    @Test
    public void testaCadastroProduto() throws Exception {
        Mockito.when(produtoService.salvarProduto(Mockito.any(Produto.class))).thenReturn(produto);
        String json = objectMapper.writeValueAsString(produto);
        mockMvc.perform(MockMvcRequestBuilders.post("/produtos")
                .contentType(MediaType.APPLICATION_JSON)
                .content(json))
                .andExpect(MockMvcResultMatchers.status().isCreated())
                .andExpect(MockMvcResultMatchers.jsonPath("$.nome", CoreMatchers.equalTo("Nome Produto teste")));
    }

    @Test
    public void testaBuscarPorId() throws Exception {
        Mockito.when(produtoService.buscarPorId(Mockito.anyInt())).thenReturn(Optional.of(produto));
        mockMvc.perform(MockMvcRequestBuilders.get("/produtos/1"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.nome", CoreMatchers.equalTo(produto.getNome())))
                .andExpect(MockMvcResultMatchers.jsonPath("$.descricao", CoreMatchers.equalTo(produto.getDescricao())))
                .andExpect(MockMvcResultMatchers.jsonPath("$.preco", CoreMatchers.equalTo(produto.getPreco())));
    }

    @Test
    public void testaBuscarPorIdNaoEncontrado() throws Exception {
        Mockito.when(produtoService.buscarPorId(Mockito.anyInt())).thenReturn(Optional.empty());
        mockMvc.perform(MockMvcRequestBuilders.get("/produtos/4"))
                .andExpect(MockMvcResultMatchers.status().isNoContent());
    }

    @Test
    public void testaDeletarProduto() throws Exception {
        Mockito.when(produtoService.buscarPorId(Mockito.anyInt())).thenReturn(Optional.of(produto));
        mockMvc.perform(MockMvcRequestBuilders.delete("/produtos/1"))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    public void testaDeletarProdutoNaoEncontrado() throws Exception {
        Mockito.when(produtoService.buscarPorId(Mockito.anyInt())).thenReturn(Optional.empty());
        mockMvc.perform(MockMvcRequestBuilders.delete("/produtos/4"))
                .andExpect(MockMvcResultMatchers.status().isNoContent());
    }

    @Test
    public void testaAtualizarProduto() throws Exception {
        Produto produtoAtualizado = new Produto();
        produtoAtualizado.setId(1);
        produtoAtualizado.setNome("Produto Atualizado");
        produtoAtualizado.setDescricao("Descrição atualizada");
        produtoAtualizado.setPreco(99.99);
        String json = objectMapper.writeValueAsString(produtoAtualizado);
        Mockito.when(produtoService.buscarPorId(Mockito.anyInt())).thenReturn(Optional.of(produto));
        Mockito.when(produtoService.atualizarProduto(Mockito.any(Produto.class))).thenReturn(produtoAtualizado);
        mockMvc.perform(MockMvcRequestBuilders.put("/produtos/1")
                .contentType(MediaType.APPLICATION_JSON)
                .content(json))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.nome", CoreMatchers.equalTo(produtoAtualizado.getNome())))
                .andExpect(MockMvcResultMatchers.jsonPath("$.descricao", CoreMatchers.equalTo(produtoAtualizado.getDescricao())))
                .andExpect(MockMvcResultMatchers.jsonPath("$.preco", CoreMatchers.equalTo(produtoAtualizado.getPreco())));
    }

    @Test
    public void testaAtualizarProdutoNaoEncontrado() throws Exception {
        Produto produtoAtualizado = new Produto();
        produtoAtualizado.setId(1);
        produtoAtualizado.setNome("Produto Atualizado");
        produtoAtualizado.setDescricao("Descrição atualizada");
        produtoAtualizado.setPreco(99.99);
        String json = objectMapper.writeValueAsString(produtoAtualizado);

        Mockito.when(produtoService.buscarPorId(Mockito.anyInt())).thenReturn(Optional.empty());
        mockMvc.perform(MockMvcRequestBuilders.put("/produtos/4")
                .contentType(MediaType.APPLICATION_JSON)
                .content(json))
                .andExpect(MockMvcResultMatchers.status().isNoContent());
    }
}
