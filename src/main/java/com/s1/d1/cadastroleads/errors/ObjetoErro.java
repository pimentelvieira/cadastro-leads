package com.s1.d1.cadastroleads.errors;

public class ObjetoErro {

    private String mensagem;
    private String campo;
    private Object parametro;

    public ObjetoErro(String mensagem, String campo, Object parametro) {
        this.mensagem = mensagem;
        this.campo = campo;
        this.parametro = parametro;
    }

    public String getMensagem() {
        return mensagem;
    }

    public void setMensagem(String mensagem) {
        this.mensagem = mensagem;
    }

    public String getCampo() {
        return campo;
    }

    public void setCampo(String campo) {
        this.campo = campo;
    }

    public Object getParametro() {
        return parametro;
    }

    public void setParametro(Object parametro) {
        this.parametro = parametro;
    }
}
