package com.s1.d1.cadastroleads.services;

import com.s1.d1.cadastroleads.models.Produto;
import com.s1.d1.cadastroleads.repositories.ProdutoRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.Optional;

@SpringBootTest
public class ProdutoServiceTest {

    @MockBean
    private ProdutoRepository produtoRepository;

    @Autowired
    private ProdutoService produtoService;

    private Produto produto;

    @BeforeEach
    public void inicializar() {
        produto = new Produto();
        produto.setId(1);
        produto.setNome("Goiaba");
        produto.setDescricao("Goiaba bonita");
        produto.setPreco(10.60);
    }

    @Test
    public void testaSalvarProduto() {
        Mockito.when(produtoRepository.save(Mockito.any(Produto.class))).thenReturn(produto);
        Produto produtoResultado = produtoService.salvarProduto(produto);

        Assertions.assertEquals(produtoResultado, produto);
        Assertions.assertEquals(produtoResultado.getNome(), produto.getNome());
    }

    @Test
    public void testaBuscaPorId() {
        Mockito.when(produtoRepository.findById(Mockito.anyInt())).thenReturn(Optional.of(produto));

        Produto produtoResultado = produtoService.buscarPorId(1).get();

        Assertions.assertEquals(produtoResultado.getNome(), produto.getNome());
        Assertions.assertEquals(produtoResultado.getDescricao(), produto.getDescricao());
        Assertions.assertEquals(produtoResultado.getPreco(), produto.getPreco());
    }

    @Test
    public void testaDeletarProduto() {
        produtoService.deletarProduto(1);
        Mockito.verify(produtoRepository).deleteById(Mockito.anyInt());
    }

    @Test
    public void testaAtualizarProduto() {
        Mockito.when(produtoRepository.save(Mockito.any(Produto.class))).thenReturn(produto);

        Produto produtoResultado = produtoService.atualizarProduto(produto);

        Assertions.assertEquals(produtoResultado, produto);
        Assertions.assertEquals(produtoResultado.getNome(), produto.getNome());
        Assertions.assertEquals(produtoResultado.getDescricao(), produto.getDescricao());
        Assertions.assertEquals(produtoResultado.getPreco(), produto.getPreco());
    }
}
