package com.s1.d1.cadastroleads.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.s1.d1.cadastroleads.enums.TipoLead;
import com.s1.d1.cadastroleads.models.Lead;
import com.s1.d1.cadastroleads.models.Produto;
import com.s1.d1.cadastroleads.services.LeadService;
import org.hamcrest.CoreMatchers;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.Arrays;
import java.util.Optional;

@WebMvcTest(LeadController.class)
public class LeadControllerTest {

    @MockBean
    private LeadService leadService;

    @Autowired
    private MockMvc mockMvc;

    private Lead lead;

    private Iterable<Produto> produtos;

    private ObjectMapper objectMapper;

    @BeforeEach
    public void inicializar() {
        lead = new Lead();
        Produto produto = new Produto();
        objectMapper = new ObjectMapper();

        produto.setId(2);
        produto.setDescricao("Produto teste");
        produto.setNome("Nome Produto teste");
        produto.setPreco(80.9);
        lead.setEmail("teste@teste.com");
        lead.setNome("Teste William");
        lead.setTipo(TipoLead.QUENTE);
        lead.setProdutos(Arrays.asList(produto));
        produtos = Arrays.asList(produto);
    }

    @Test
    public void testaCadastroLead() throws Exception {
        Mockito.when(leadService.adicionarLead(Mockito.any(Lead.class))).thenReturn(lead);
        Mockito.when(leadService.buscarTodosProdutos(Mockito.anyList())).thenReturn(produtos);
        String json = objectMapper.writeValueAsString(lead);
        mockMvc.perform(MockMvcRequestBuilders.post("/leads")
                .contentType(MediaType.APPLICATION_JSON)
                .content(json))
                .andExpect(MockMvcResultMatchers.status().isCreated())
                .andExpect(MockMvcResultMatchers.jsonPath("$.produtos[0].id", CoreMatchers.equalTo(2)));
    }

    @Test
    public void testaBuscarPorId() throws Exception {
        Mockito.when(leadService.buscarPorId(Mockito.anyInt())).thenReturn(Optional.of(lead));
        mockMvc.perform(MockMvcRequestBuilders.get("/leads/1"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.nome", CoreMatchers.equalTo(lead.getNome())))
                .andExpect(MockMvcResultMatchers.jsonPath("$.email", CoreMatchers.equalTo(lead.getEmail())))
                .andExpect(MockMvcResultMatchers.jsonPath("$.tipo", CoreMatchers.equalTo(lead.getTipo().name())));
    }

    @Test
    public void testaBuscarPorIdNaoEncontrado() throws Exception {
        Mockito.when(leadService.buscarPorId(Mockito.anyInt())).thenReturn(Optional.empty());
        mockMvc.perform(MockMvcRequestBuilders.get("/leads/4"))
                .andExpect(MockMvcResultMatchers.status().isNoContent());
    }

    @Test
    public void testaDeletarLead() throws Exception {
        Mockito.when(leadService.buscarPorId(Mockito.anyInt())).thenReturn(Optional.of(lead));
        mockMvc.perform(MockMvcRequestBuilders.delete("/leads/1"))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    public void testaDeletarLeadNaoEncontrado() throws Exception {
        Mockito.when(leadService.buscarPorId(Mockito.anyInt())).thenReturn(Optional.empty());
        mockMvc.perform(MockMvcRequestBuilders.delete("/leads/4"))
                .andExpect(MockMvcResultMatchers.status().isNoContent());
    }

    @Test
    public void testaAtualizarLead() throws Exception {
        Lead leadAtualizado = new Lead();
        leadAtualizado.setId(1);
        leadAtualizado.setNome("Lead Atualizado");
        leadAtualizado.setEmail("lead@lead.com");
        leadAtualizado.setTipo(TipoLead.FRIO);
        String json = objectMapper.writeValueAsString(leadAtualizado);
        Mockito.when(leadService.buscarPorId(Mockito.anyInt())).thenReturn(Optional.of(lead));
        Mockito.when(leadService.atualizarLead(Mockito.any(Lead.class))).thenReturn(leadAtualizado);
        mockMvc.perform(MockMvcRequestBuilders.put("/leads/1")
                .contentType(MediaType.APPLICATION_JSON)
                .content(json))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.nome", CoreMatchers.equalTo(leadAtualizado.getNome())))
                .andExpect(MockMvcResultMatchers.jsonPath("$.email", CoreMatchers.equalTo(leadAtualizado.getEmail())))
                .andExpect(MockMvcResultMatchers.jsonPath("$.tipo", CoreMatchers.equalTo(leadAtualizado.getTipo().name())));
    }

    @Test
    public void testaAtualizarLeadNaoEncontrado() throws Exception {
        Lead leadAtualizado = new Lead();
        leadAtualizado.setId(1);
        leadAtualizado.setNome("Lead Atualizado");
        leadAtualizado.setEmail("lead@lead.com");
        leadAtualizado.setTipo(TipoLead.FRIO);
        String json = objectMapper.writeValueAsString(leadAtualizado);

        Mockito.when(leadService.buscarPorId(Mockito.anyInt())).thenReturn(Optional.empty());
        mockMvc.perform(MockMvcRequestBuilders.put("/leads/4")
                .contentType(MediaType.APPLICATION_JSON)
                .content(json))
                .andExpect(MockMvcResultMatchers.status().isNoContent());
    }

    @Test
    public void testaCadastroLeadComEmailInvalido() throws Exception {
        lead.setEmail("teste");
        Mockito.when(leadService.adicionarLead(Mockito.any(Lead.class))).thenReturn(lead);
        Mockito.when(leadService.buscarTodosProdutos(Mockito.anyList())).thenReturn(produtos);
        String json = objectMapper.writeValueAsString(lead);
        mockMvc.perform(MockMvcRequestBuilders.post("/leads")
                .contentType(MediaType.APPLICATION_JSON)
                .content(json))
                .andExpect(MockMvcResultMatchers.status().isBadRequest())
                .andExpect(MockMvcResultMatchers.jsonPath("$.erros[0].mensagem", CoreMatchers.equalTo("Email inválido")));
    }

    @Test
    public void testaCadastroLeadComNomeMenor() throws Exception {
        lead.setNome("teste");
        Mockito.when(leadService.adicionarLead(Mockito.any(Lead.class))).thenReturn(lead);
        Mockito.when(leadService.buscarTodosProdutos(Mockito.anyList())).thenReturn(produtos);
        String json = objectMapper.writeValueAsString(lead);
        mockMvc.perform(MockMvcRequestBuilders.post("/leads")
                .contentType(MediaType.APPLICATION_JSON)
                .content(json))
                .andExpect(MockMvcResultMatchers.status().isBadRequest())
                .andExpect(MockMvcResultMatchers.jsonPath("$.erros[0].mensagem", CoreMatchers.equalTo("Nome deve ter no minimo 8 caracteres e no máximo 100 caracteres")));
    }

    @Test
    public void testaCadastroLeadComNomeMaior() throws Exception {
        lead.setNome("testetestetestetestetestetestetestetestetestetestetestetestetestetestetestetestetestetestetestetestetestetestetestetestetestetestetestetestetestetestetesteteste");
        Mockito.when(leadService.adicionarLead(Mockito.any(Lead.class))).thenReturn(lead);
        Mockito.when(leadService.buscarTodosProdutos(Mockito.anyList())).thenReturn(produtos);
        String json = objectMapper.writeValueAsString(lead);
        mockMvc.perform(MockMvcRequestBuilders.post("/leads")
                .contentType(MediaType.APPLICATION_JSON)
                .content(json))
                .andExpect(MockMvcResultMatchers.status().isBadRequest())
                .andExpect(MockMvcResultMatchers.jsonPath("$.erros[0].mensagem", CoreMatchers.equalTo("Nome deve ter no minimo 8 caracteres e no máximo 100 caracteres")));
    }

    @Test
    public void testaCadastroLeadComNomeNull() throws Exception {
        lead.setNome(null);
        Mockito.when(leadService.adicionarLead(Mockito.any(Lead.class))).thenReturn(lead);
        Mockito.when(leadService.buscarTodosProdutos(Mockito.anyList())).thenReturn(produtos);
        String json = objectMapper.writeValueAsString(lead);
        mockMvc.perform(MockMvcRequestBuilders.post("/leads")
                .contentType(MediaType.APPLICATION_JSON)
                .content(json))
                .andExpect(MockMvcResultMatchers.status().isBadRequest());
    }
}
