package com.s1.d1.cadastroleads.controllers;

import com.s1.d1.cadastroleads.models.Produto;
import com.s1.d1.cadastroleads.services.ProdutoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.Optional;

@RestController
@RequestMapping("/produtos")
public class ProdutoController {

    @Autowired
    private ProdutoService service;

    @GetMapping("/{id}")
    public Produto buscarPorIndice(@PathVariable Integer id) {
        Optional<Produto> produto = this.service.buscarPorId(id);

        if (produto.isPresent()) {
            return produto.get();
        } else {
            throw new ResponseStatusException(HttpStatus.NO_CONTENT);
        }
    }

    @PostMapping
    public ResponseEntity<Produto> criarProduto(@RequestBody Produto produto) {
        Produto produtoCriado = service.salvarProduto(produto);
        return ResponseEntity.status(HttpStatus.CREATED).body(produtoCriado);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity deletarLead(@PathVariable Integer id) {
        Optional<Produto> produto = this.service.buscarPorId(id);

        if (produto.isPresent()) {
            this.service.deletarProduto(id);
            return ResponseEntity.status(HttpStatus.OK).build();
        } else {
            throw new ResponseStatusException(HttpStatus.NO_CONTENT, "Produto não encontrado para exclusão");
        }
    }

    @PutMapping("/{id}")
    public ResponseEntity<Produto> atualizarLead(@PathVariable Integer id, @RequestBody @Valid Produto produto) {
        Optional<Produto> produtoRetornado = this.service.buscarPorId(id);

        if (produtoRetornado.isPresent()) {
            produto.setId(id);
            Produto produtoAtualizado = this.service.atualizarProduto(produto);
            return ResponseEntity.status(HttpStatus.OK).body(produtoAtualizado);
        } else {
            throw new ResponseStatusException(HttpStatus.NO_CONTENT, "Produto não encontrado para atualização");
        }
    }
}
