package com.s1.d1.cadastroleads.services;

import com.s1.d1.cadastroleads.enums.TipoLead;
import com.s1.d1.cadastroleads.models.Lead;
import com.s1.d1.cadastroleads.models.Produto;
import com.s1.d1.cadastroleads.repositories.LeadRepository;
import com.s1.d1.cadastroleads.repositories.ProdutoRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

@SpringBootTest
public class LeadServiceTest {

    @MockBean
    private LeadRepository leadRepository;

    @MockBean
    private ProdutoRepository produtoRepository;

    @Autowired
    private LeadService leadService;

    private Lead lead;

    @BeforeEach
    public void inicializar() {
        lead = new Lead();
        Produto produto = new Produto();
        produto.setId(2);
        produto.setDescricao("Produto teste");
        produto.setNome("Nome Produto teste");
        produto.setPreco(80.9);
        lead.setId(1);
        lead.setEmail("teste@teste.com");
        lead.setNome("Teste");
        lead.setTipo(TipoLead.QUENTE);
        lead.setProdutos(Arrays.asList(produto));
    }

    @Test
    public void testaSalvarLead() {
        Mockito.when(leadRepository.save(Mockito.any(Lead.class))).thenReturn(lead);
        Lead leadResultado = leadService.adicionarLead(lead);

        Assertions.assertEquals(leadResultado, lead);
        Assertions.assertEquals(leadResultado.getEmail(), lead.getEmail());
    }

    @Test
    public void testaBuscarTodosProdutos() {
        Mockito.when(produtoRepository.findAllById(Mockito.anyIterable())).thenReturn(lead.getProdutos());

        Iterable<Produto> result = leadService.buscarTodosProdutos(Arrays.asList(2));

        Assertions.assertEquals(((List) result).size(), 1);
        Assertions.assertEquals(((List<Produto>) result).get(0).getNome(), "Nome Produto teste");
        Assertions.assertEquals(((List<Produto>) result).get(0).getDescricao(), "Produto teste");
    }

    @Test
    public void testaBuscaPorId() {
        Mockito.when(leadRepository.findById(Mockito.anyInt())).thenReturn(Optional.of(lead));

        Lead leadResultado = leadService.buscarPorId(1).get();

        Assertions.assertEquals(leadResultado.getNome(), "Teste");
        Assertions.assertEquals(leadResultado.getEmail(), "teste@teste.com");
    }

    @Test
    public void testaDeletarLead() {
        leadService.deletarLead(1);
        Mockito.verify(leadRepository).deleteById(Mockito.anyInt());
    }

    @Test
    public void testaAtualizarLead() {
        Mockito.when(leadRepository.save(Mockito.any(Lead.class))).thenReturn(lead);

        Lead leadResultado = leadService.atualizarLead(lead);

        Assertions.assertEquals(leadResultado, lead);
        Assertions.assertEquals(leadResultado.getEmail(), lead.getEmail());
        Assertions.assertEquals(leadResultado.getNome(), lead.getNome());
    }
}
