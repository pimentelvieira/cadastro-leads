package com.s1.d1.cadastroleads.services;

import com.s1.d1.cadastroleads.models.Lead;
import com.s1.d1.cadastroleads.models.Produto;
import com.s1.d1.cadastroleads.repositories.LeadRepository;
import com.s1.d1.cadastroleads.repositories.ProdutoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class LeadService {

    @Autowired
    private LeadRepository leadRepository;

    @Autowired
    private ProdutoRepository produtoRepository;

    public Iterable<Produto> buscarTodosProdutos(List<Integer> produtosId) {
        return this.produtoRepository.findAllById(produtosId);
    }

    public Optional<Lead> buscarPorId(Integer id) {
        return this.leadRepository.findById(id);
    }

    public Lead adicionarLead(Lead lead) {
        return this.leadRepository.save(lead);
    }

    public void deletarLead(Integer leadId) {
        this.leadRepository.deleteById(leadId);
    }

    public Lead atualizarLead(Lead lead) {
        return this.leadRepository.save(lead);
    }
}
