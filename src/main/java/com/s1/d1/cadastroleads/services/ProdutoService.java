package com.s1.d1.cadastroleads.services;

import com.s1.d1.cadastroleads.models.Lead;
import com.s1.d1.cadastroleads.models.Produto;
import com.s1.d1.cadastroleads.repositories.ProdutoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class ProdutoService {

    @Autowired
    private ProdutoRepository repository;

    public Produto salvarProduto(Produto produto) {
        return repository.save(produto);
    }

    public Optional<Produto> buscarPorId(Integer id) {
        return this.repository.findById(id);
    }

    public void deletarProduto(Integer leadId) {
        this.repository.deleteById(leadId);
    }

    public Produto atualizarProduto(Produto produto) {
        return this.repository.save(produto);
    }
}
