package com.s1.d1.cadastroleads.repositories;

import com.s1.d1.cadastroleads.models.Produto;
import org.springframework.data.repository.CrudRepository;

public interface ProdutoRepository extends CrudRepository<Produto, Integer> {
}
